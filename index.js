// console.log("Hello World");

let username;
let password;
let role;

function login(){
	username = prompt("username:");
	password = prompt("password:");
	role = prompt("role:").toLowerCase();
	if (username == "" || password == "" || role == ""){
		alert("input should not be empty");
	}
	else {
		switch(role){
			case "admin":
				console.log("Welcome back to the class portal, admin!");
				break;
			case "teacher":
				console.log("Thank you for logging in, teacher!");
				break;
			case "student":
				console.log("Welcome to the class portal, student!");
				break;
			default:
				console.log("Role out of range.");
		}
	}
};

login();

function checkAverage(num1, num2, num3, num4){
	let average = Math.round((num1 + num2 + num3 + num4) / 4);
	if(role != "student"){
		alert(role.toUpperCase() + "! You are not allowed to access this feature!")
	}
	else{
		if(average <= 74){
			console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
		}
		else if(average >= 75 && average <= 79){
			console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
		}
		else if(average >= 80 && average <= 84){
			console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
		}
		else if(average >= 85 && average <= 89){
			console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
		}
		else if(average >= 90 && average <= 95){
			console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
		}
		else if(average >= 96){
			console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
		}
	}
};


